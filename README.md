      __  __            _                      _             
     |  \/  |          | |                    | |            
     | \  / | __ _ _ __| | ___   _ _ __   __ _| |_ ___  _ __ 
     | |\/| |/ _` | '__| |/ / | | | '_ \ / _` | __/ _ \| '__|
     | |  | | (_| | |  |   <| |_| | |_) | (_| | || (_) | |   
     |_|  |_|\__,_|_|  |_|\_\\__,_| .__/ \__,_|\__\___/|_|   
                                  | |                        
                                  |_|                         
[![npm version](https://badge.fury.io/js/markupator.svg)](https://badge.fury.io/js/markupator)
[![pipeline status](https://gitlab.com/coderman/markupator/badges/master/pipeline.svg)](https://gitlab.com/coderman/markupator/-/commits/master)
![npm bundle size](https://img.shields.io/bundlephobia/min/markupator)

# Markupator

Library that mimics _React's_ functions for creating HTML elements and attaching them to the page.

## Rquirements
Node version `14`.


## Installation

Install it via NPM by using the following command:

```
npm install markupator --save-dev
```

## Usage

Import the functions you need like so:

```
import { createElement, render } from 'markupator';
```

### Creating HTML elements

`createElement` let's you quickly create an HTML element:

```
const element = createElement(tag, attribs, ...children);
```

#### Parameters

* `type` [required]: The type argument must be a valid HTML tag type. For example, it could be a tag name string (such as 'div' or 'span').
* `attribs` [required]: The attribs argument must either be an object or null. If you pass null, it will be treated the same as an empty object. 
* `...children` [optional]: Zero or more child nodes. They can be any HTML nodes, including text and arrays of objects.

#### Examples

To create the following markup:

```
<h1 class="title" area-label="title">Hello World</h1>
```

you would type: `createElement('h1', { class: 'title', areaLabel: 'title', 'Hello World');`

### Attaching HTML elements

`render` appends elements created via `createElement` function into a browser DOM node.

```
render(domNode, domNode)
```

#### Parameters

* `domNode` - newly created element/s [required]: First argument is the newly created element or elements you with to add to your DOM.
* `domNode` - target [required]: This is existing HTML element you wish to append your newly created elements to. 

#### Examples

Create new element, define an existing target element and append to it:

```
const element = createElement('h1', null, 'Hi');
const target = document.querySelector('.header');

render(element, target);
```

##### From
```
<header class="header"></header>
```

##### To
```
<header class="header><h1>Hi</h1></header>
```