/**
 * @jest-environment jsdom
 */

import { createElement } from './index.js';

test('Creates a single divider', () => {
  expect(createElement('div').outerHTML).toBe('<div></div>');
});

test('Creates a single divider with a class name', () => {
  expect(createElement('div', { class: 'container' }).outerHTML).toBe('<div class="container"></div>');
});

test('Creates a single heading with a class name, attribute and text', () => {
  expect(createElement('h1', { class: 'title', areaLabel: 'hey' }, 'Hello').outerHTML).toBe('<h1 class="title" area-label="hey">Hello</h1>');
});

test('Creates a single image tag (self-closing)', () => {
  expect(createElement('img', { src: 'image.jpg' }).outerHTML).toBe('<img src="image.jpg">');
});

test('Creates paragraph with a span iside as well as some text', () => {
  expect(createElement('p', null, 'Hello ', createElement('span', null, 'World')).outerHTML).toBe('<p>Hello <span>World</span></p>');
});

test('Creates list with items and text', () => {
  const food = ['milk', 'bread'].map(item => {
    return createElement('li', null, item);
  });

  expect(
    createElement(
      'ul',
      null,
      ...food
    ).outerHTML
  ).toBe('<ul><li>milk</li><li>bread</li></ul>');
});

