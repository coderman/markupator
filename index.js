/**
 * Create an HTML tag
 * @param  {string} tag      HTML tag, like: 'h1', 'p'...
 * @param  {Object} attr     Attributes as key value pairs.
 * @param  {String} children Child elements. Can be text or other node elements.
 * @return {String}          Generated HTML code.
 */
const createElement = (tag, attr = null, ...children) => {
  const root = document.createElement(tag);

  const isSelfClosing = (tagName) => {
    const selfClosingTags = [
      'area',
      'base',
      'br',
      'col',
      'embed',
      'hr',
      'img',
      'input',
      'link',
      'meta',
      'param',
      'source',
      'track',
      'wbr',
    ];

    if (selfClosingTags.find((selfClosingTag) => tagName === selfClosingTag)) return true;

    return false;
  };

  const isObjectEmpty = (objectName) => {
    return (
      objectName
      && Object.keys(objectName).length === 0
      && objectName.constructor === Object
    );
  };

  /**
   * Converts camel case to kebab case: 'someKey' -> 'some-key'
   * @param  {String} str Camel case to be converted.
   * @return {String}     Kebab case.
   */
  const camelToKebabCase = (str) => str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);

  if (!isObjectEmpty(attr)) {
    for (const prop in attr) {
      root.setAttribute(camelToKebabCase(prop), attr[prop]);
    }
  }

  if (!isSelfClosing(tag)) {
    // Append children to the parent element.
    root.append(...children);
  }

  return root;
};

/**
 * Attaches the HTML string to a target.
 * @param  {DOM Object} target Target HTML element (node).
 * @param  {String} el     Markup to be injected.
 */
const render = (el, target) => target.appendChild(el);

export { createElement, render };
